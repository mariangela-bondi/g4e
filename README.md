Geant 4 EIC (g4e)
=================

[![Documentation Status](https://readthedocs.org/projects/g4e/badge/?version=latest)](https://g4e.readthedocs.io/en/latest/?badge=latest)

![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/jlab-eic/g4e/master)

[The documentation is at g4e.readthedocs.io](https://g4e.readthedocs.org/)


![JLEIC detector](docs/_images/JLEICgeant4-v1a.png)



